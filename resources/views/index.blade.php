<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>

      nav {
         background: rgba(255, 172, 140, 0.966);
         height: 10%;
         font-size: 20px;
         text-align: center;
         padding-top: 20px;
         padding: 10px;
       }

       header {
         background: rgba(0, 238, 255, 0.966);
         height: 10%;
         font-size: 30px;
         text-align: center;
         padding-top: 20px;
         padding: 10px;
       }

       aside {
          background: rgb(255, 96, 136);
          float: left;
          height: 400px;
          width: 30%;
          font-size: 20px;
          text-align: center;
          padding-top: 20px;
       }

       article {
          background: rgb(255, 72, 0);
          float: right;
          width: 70%;
          height: 400px;
          font-size: 20px;
          text-align: center;
          padding-top: 20px;
       }

       footer {
          background: rgb(0, 225, 255);
          text-align: center;
          font-size: 15px;
       }

    </style>
</head>
<body>
    <!-- nav digunakan untuk tautan navigasi utama -->
    <nav>
        <a href="./home">Home</a>
        <a href="./signup">Sign Up</a>
        <a href="./login">Log In</a>
     </nav>

     <!-- header adalah bagian tajuk dari sebuah halaman web -->
     <header>
        <h1>This is my website</h1>
        <h3>Please login to continue</h3>
     </header>

     <!-- aside adalah bagian samping konten utama -->
     <aside>
        <div>
           Hello, anonymous!
        </div>
        <a href="./terms">Profile</a>
     </aside>

     <!-- article menandakan sebuah blok teks yang isinya independen terhadap element lain -->
     <article>
        <!-- section menandakan bagian dalam sebuah halaman web -->
        <section>
           <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong> 
        </section>
        
        <section>
           Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
         </section>
     </article>

     <!-- footer merupakan bagian halaman web di bawah konten utama -->
     <footer>2021 Copyright</footer>

</body>
</html>